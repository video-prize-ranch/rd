# rd
Automatically redirect to a random frontend instance quickly and privately.

## Instances
Only instances that don't use Cloudflare are used.

- **rimgo**: ri.bcow.xyz
  - i.bcow.xyz
  - rimgo.pussthecat.org
  - rimgo.totaldarkness.net
  - rimgo.bus-hit.me
  - rimgo.esmailelbob.xyz
  - rimgo.privacydev.net
  - imgur.artemislena.eu
  - rimgo.vern.cc
- **Librarian**: rl.bcow.xyz
  - lbry.bcow.xyz
  - librarian.pussthecat.org
  - lbry.mutahar.rocks
  - librarian.esmailelbob.xyz
  - lbry.vern.cc

## Usage
Replace odysee.com or imgur.com with the domain for the service above. You will also need to add a # (this hides the URL from the server) to the end like this:
```
https://ri.bcow.xyz/#a/H8M4rcp
https://rl.bcow.xyz/#@SomeOrdinaryGamers:a
```